#include "ros/ros.h"
#include "sensor_msgs/Image.h"

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "jetmap.h"

double max_depth;
int jetmap_max;
ros::Publisher depthPublisher;

void callback(const sensor_msgs::Image::ConstPtr& depth){
    cv_bridge::CvImagePtr cv_ptr;
    cv_ptr = cv_bridge::toCvCopy(depth);
    IplImage *D = new IplImage(cv_ptr->image);

    IplImage *color_depth = cvCreateImage(cvSize(depth->width,depth->height),IPL_DEPTH_8U,3);
    float d;
    int col;
    for(int i=0;i<depth->width;i++){
        for(int j=0;j<depth->height;j++){
            d=cvGet2D(D,j,i).val[0];
            if(!std::isnormal(d)) {
                col=0;
            }
            else if(d>max_depth){
                col =jetmap_max;
            }
            else {
                col = (int)(jetmap_max*(d/max_depth));
            }
            CvScalar color=CV_RGB(jetmap[col][0],jetmap[col][1],jetmap[col][2]);
            cvSet2D(color_depth,j,i,color);
        }
    }

    cv::Mat imageMat(color_depth);

    cv_bridge::CvImage out_msg;
    out_msg.encoding = depth->encoding; 
    out_msg.image    = imageMat;
    out_msg.header.seq      = depth->header.seq;
    out_msg.header.frame_id = depth->header.frame_id;
    out_msg.header.stamp    = depth->header.stamp;
    
    depthPublisher.publish(out_msg.toImageMsg());
    cvReleaseImage(&color_depth);
    cvReleaseImage(&D);
    return;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "depth2colormap");
    ros::NodeHandle nh;
    ros::NodeHandle np("~");
    np.param("max_depth",max_depth, double(7.0));
    jetmap_max=sizeof(jetmap)/sizeof(int)/3-1;
    ROS_INFO_STREAM("jetmap_max "<<jetmap_max);
    ros::Subscriber depth_sub = nh.subscribe("input/depth", 1, callback);
    depthPublisher=nh.advertise<sensor_msgs::Image>("/output/depth_colormap", 2);
    ros::spin();
    return 0;
}
